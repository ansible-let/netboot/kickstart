text
lang en_US.UTF-8
keyboard us
skipx
timezone --utc Europe/Zurich --ntpservers swisstime.ethz.ch
# Disk
auth --enableshadow --passalgo=sha512
selinux --enforcing
firewall --enabled --service=ssh
firstboot --disable
# Network information
network  --bootproto=dhcp --device=link --activate --onboot=on
# Root password
services --disabled="kdump,rhsmcertd" --enabled="NetworkManager,sshd,rsyslog,chronyd,rngd"
rootpw --iscrypted $5$6mVDkPOCONHrj3LV$ipjLEPQe/V4854EEZj5ASPvj2QDETHu2Y54NbWh6zH8
url --url https://download.rockylinux.org/stg/rocky/9/BaseOS/x86_64/os/

bootloader --location=mbr --append="nofb quiet splash=quiet" 
zerombr
clearpart --all --initlabel
autopart 

reboot

%packages
@core
rocky-release
dnf
kernel
yum
nfs-utils
dnf-utils
hostname
-aic94xx-firmware
-alsa-firmware
-alsa-lib
-alsa-tools-firmware
-ivtv-firmware
-iwl1000-firmware
-iwl100-firmware
-iwl105-firmware
-iwl135-firmware
-iwl2000-firmware
-iwl2030-firmware
-iwl3160-firmware
-iwl3945-firmware
-iwl4965-firmware
-iwl5000-firmware
-iwl5150-firmware
-iwl6000-firmware
-iwl6000g2a-firmware
-iwl6000g2b-firmware
-iwl6050-firmware
-iwl7260-firmware
-libertas-sd8686-firmware
-libertas-sd8787-firmware
-libertas-usb8388-firmware

python3-jsonschema
dracut-config-generic
dracut-config-rescue
firewalld

# some stuff that's missing from core or things we want
tar
tcpdump
rsync
rng-tools
qemu-guest-agent
virt-what

-biosdevname
-plymouth
-iprutils
# Fixes an s390x issue
#-langpacks-*
-langpacks-en
%end



%post --erroronfail

# Attempting to force legacy BIOS boot if we boot from UEFI
if [ "$(arch)" = "x86_64"  ]; then
  dnf install grub2-pc-modules grub2-pc -y
  grub2-install --target=i386-pc /dev/vda
fi

# setup systemd to boot to the right runlevel
rm -f /etc/systemd/system/default.target
ln -s /lib/systemd/system/multi-user.target /etc/systemd/system/default.target
echo .

# we don't need this in virt
dnf -C -y remove linux-firmware
dnf -C -y remove avahi\* 

cat > /etc/sysconfig/network << EOF
NETWORKING=yes
NOZEROCONF=yes
EOF

# this should *really* be an empty file - gotta make anaconda happy
truncate -s 0 /etc/resolv.conf

# For cloud images, 'eth0' _is_ the predictable device name, since
# we don't want to be tied to specific virtual (!) hardware
rm -f /etc/udev/rules.d/70*
ln -s /dev/null /etc/udev/rules.d/80-net-name-slot.rules
rm -f /etc/sysconfig/network-scripts/ifcfg-*

# simple eth0 config, again not hard-coded to the build hardware
cat > /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DEVICE="eth0"
BOOTPROTO="dhcp"
BOOTPROTOv6="dhcp"
ONBOOT="yes"
TYPE="Ethernet"
USERCTL="yes"
PEERDNS="yes"
IPV6INIT="yes"
PERSISTENT_DHCLIENT="1"
EOF

echo "virtual-guest" > /etc/tuned/active_profile

# generic localhost names
cat > /etc/hosts << EOF
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

EOF
echo .

#systemctl mask tmp.mount

cat <<EOL > /etc/sysconfig/kernel
# UPDATEDEFAULT specifies if new-kernel-pkg should make
# new kernels the default
UPDATEDEFAULT=yes

# DEFAULTKERNEL specifies the default kernel package type
DEFAULTKERNEL=kernel
EOL

# make sure firstboot doesn't start
echo "RUN_FIRSTBOOT=NO" > /etc/sysconfig/firstboot

# these shouldn't be enabled, but just in case
sed -i 's|^enabled=1|enabled=0|' /etc/yum/pluginconf.d/product-id.conf
sed -i 's|^enabled=1|enabled=0|' /etc/yum/pluginconf.d/subscription-manager.conf

dnf clean all

# XXX instance type markers - MUST match Rocky Infra expectation
echo 'genclo' > /etc/yum/vars/infra

echo "Updating system time"
systemctl enable --now chronyd
/usr/bin/chronyc -a makestep
/usr/sbin/hwclock --systohc

# SSH access
mkdir -p ~root/.ssh
cat << EOF >> ~root/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK4ViMtAG+UOKBpFWSFnARRfydVMze0H3qP/CyOI60hE beatmue
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJeolp0Z8wPeb6vuCaClVPrIBGm44cX3sIqBjnjHPclH mkern
ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAYEAhsrIP7evOAd+l7uzclDRfHQ7SMkyPzAGKx9W0KIu7v9pmvGMiuPLOYIgYm9aTDs6ArsbuT+rCZpnwsAbwWH2jPaZXkgiwkHHRaiJ6HX6tpChfLwAi0zGiH+Ut2BWZb/Jh8A21mW+B3KPfzNBIAQAZgUp3HURmtBx4pIZkE5+YDYLDPfB1jJ0ZTlnSijqwDm9N8aYKmQ567lnAVMYH0Xh+7cPYtNamJbfVyArocBo0r26nNx9cojZXmWKnCs8Yp0/wxYCqVmLjWQYZfFcpNisEHT4lPQevaLZP+F4onOBGuPEVOptxiRQ/DGNRTv2gGXHeuKIWznc6HDLZU2DOWVpmtsdMBsfP8UssasABxKc3kDiGI9kzgmj9gB4+GqoWobDF61MT2gOwXWRQXi5P5beiLk1lu/f4qaKQEu3088f2oaC8QyKAQwfrS1mresgTDQNpa9tK9VY6DeKQpc366cVSyGZdcswpsNgXa1pfMt3ba/GkM86djMAg5r9sszEUqdz bgiger@ethz.ch
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDrU3bfGL6XR30bV4ZFknQrxbLeqN/5teKQCcwHoHD2x miodermatt
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINjmO0RZBBJZj3I1srIgg6jJyGTJoXBBNzePgiv1ftaY thluethi
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKVmLPNJHqyeDsMR9uoq+2Z8mJU4w9dghJhjUP1u6buK fsteger
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDN1MkSKknFX0g09GF5T0QisGUgrsIg2tUxKF27HXiePNmbxsQXf20fBuPqvwAaGNaG/JyBtV4Pn6sgJvAi/Tkk7vu2FQCh/CL2fGhjQKD8ap9ieOCpVYIXTUL0d423zL1lIyf/1XUtYXeexoKv/SjTGXtSitntVQ7NO/aE1goeLz9x85K+z3JNwy4iNVlbGSHbBQsDj5ZP5zXPJoJNw0eiLGMCzWfAhs8NUpdP0nPA8hKiBD/cJmNS8Od+CkIfz4A+9pwgl3GR+6pxn8p0U6bUsSwKHWEbMeksbumyFd9EEMs1bq5sNsNdVo+B/G/YZ7oIULln3H5yxgGlTlOdcYQKBRz8Tq4hfN28tyNiR2dblHd1o+U/MMDJajRQXRxUCkhgHAi7IXUw/uFgC5/RVSnQIBF47K/qbLo5SWBbXfpZff8EeRbcRrQOxfmM0N8W2BavxQB5GNftXUVAjqV4empf8BWjIED7gra5D0z62J9s3noBxVKEacexlZ11q688FXvH3qoT+kggHP2ykiFwH/QCDbWeSNttPDx/nP2WlorRrvCyHE4mT5BVDQpTGQiUl14DxeQIygEXRi3hYyaa4J97bdjuXcqck6kPPKaSo8UbsmxLjlzhvNw1yOGcjOvcMM8hf1OJr2nZMhA9yboKPS1VsHbTwgMn3ccrcHtYl+v7eQ== ansible
EOF

chmod 0700 ~root/.ssh
chmod 0600 ~root/.ssh/authorized_keys
chown -R root: ~root/.ssh

rm -rf /var/log/yum.log
rm -rf /var/lib/yum/*
rm -rf /root/install.log
rm -rf /root/install.log.syslog
rm -rf /root/anaconda-ks.cfg
rm -rf /var/log/anaconda*

echo "Fixing SELinux contexts."
touch /var/log/cron
touch /var/log/boot.log
mkdir -p /var/cache/yum
/usr/sbin/fixfiles -R -a restore

rm -f /var/lib/systemd/random-seed
cat /dev/null > /etc/machine-id

# reorder console entries
#sed -i 's/console=tty0/console=tty0 console=ttyS0,115200n8/' /boot/grub2/grub.cfg

# LVM Variant Fix, remove system.devices
rm -fv /etc/lvm/devices/system.devices

true

%end
