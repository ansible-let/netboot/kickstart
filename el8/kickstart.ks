


# This kickstart file was rendered from the Foreman provisioning template "Kickstart default".



url --url http://id-sat-prd-02.ethz.ch/pulp/repos/COMMON/Library/content/dist/rhel8/8.8/x86_64/baseos/kickstart/
repo --name Red_Hat_Enterprise_Linux_8_for_x86_64_-_AppStream_Kickstart_8 --baseurl http://id-sat-prd-02.ethz.ch/pulp/repos/COMMON/Library/content/dist/rhel8/8.8/x86_64/appstream/kickstart/ 


lang en_US.UTF-8
selinux --enforcing
keyboard us
skipx

network --device=00:50:56:ae:49:2c --hostname duna.ethz.ch --noipv6 --bootproto static --ip=129.132.150.131 --netmask=255.255.255.128 --gateway=129.132.150.129 --mtu=1500 --nameserver=129.132.98.12,129.132.250.2


rootpw --iscrypted $5$6mVDkPOCONHrj3LV$ipjLEPQe/V4854EEZj5ASPvj2QDETHu2Y54NbWh6zH8
firewall --service=ssh
authselect --useshadow --passalgo=sha256 --kickstart
timezone --utc Europe/Zurich --ntpservers swisstime.ethz.ch

services --disabled gpm,sendmail,cups,pcmcia,isdn,rawdevices,hpoj,bluetooth,openibd,avahi-daemon,avahi-dnsconfd,hidd,hplip,pcscd



bootloader --location=mbr --append="nofb quiet splash=quiet" 


zerombr
clearpart --all --initlabel
autopart 


text
reboot

%packages
# Set parameter 'custompackages' to be included here
python36
perl
yum
dhclient
chrony
-ntp
wget
@Core
redhat-lsb-core
%end

%post --nochroot
exec < /dev/tty3 > /dev/tty3
#changing to VT 3 so that we can see whats going on....
/usr/bin/chvt 3
(
cp -va /etc/resolv.conf /mnt/sysimage/etc/resolv.conf
/usr/bin/chvt 1
) 2>&1 | tee /mnt/sysimage/root/install.postnochroot.log
%end

%post --log=/root/install.post.log
logger "Starting anaconda postinstall"
exec < /dev/tty3 > /dev/tty3
#changing to VT 3 so that we can see whats going on....
/usr/bin/chvt 3





# ens192 interface
real=`grep -l 00:50:56:ae:49:2c /sys/class/net/*/{bonding_slave/perm_hwaddr,address} 2>/dev/null | awk -F '/' '// {print $5}' | head -1`
sanitized_real=`echo $real | sed s/:/_/`


cat << EOF > /etc/sysconfig/network-scripts/ifcfg-$sanitized_real
BOOTPROTO="none"
IPADDR="129.132.150.131"
NETMASK="255.255.255.128"
GATEWAY="129.132.150.129"
DOMAIN="ethz.ch"
DEVICE=$real
HWADDR="00:50:56:ae:49:2c"
ONBOOT=yes
PEERDNS=yes
PEERROUTES=yes
DEFROUTE=yes
DNS1="129.132.98.12"
DNS2="129.132.250.2"
MTU=1500
EOF







echo "Updating system time"
systemctl enable --now chronyd
/usr/bin/chronyc -a makestep
/usr/sbin/hwclock --systohc












  

  echo "##############################################################"
  echo "################# SUBSCRIPTION MANAGER #######################"
  echo "##############################################################"
  echo
  echo "Starting the subscription-manager registration process"

  
    if [ -f /usr/bin/dnf ]; then
      dnf -y install subscription-manager
    else
      yum -t -y install subscription-manager
    fi
  



  

  
    rpm -Uvh http://id-sat-prd.ethz.ch/pub/katello-ca-consumer-latest.noarch.rpm
  

  
    subscription-manager register --name="duna.ethz.ch" --org='LET' --activationkey='LET-RHEL8-testing'
  

  

  

  

  
    
       if [ -f /usr/bin/dnf ]; then
         PACKAGE_MAN="dnf -y"
       else
         PACKAGE_MAN="yum -t -y"
       fi
    

    
      $PACKAGE_MAN install katello-agent
    

    
  






# update all the base packages from the updates repository
if [ -f /usr/bin/dnf ]; then
  dnf -y update
else
  yum -t -y update
fi


# SSH keys setup snippet for Remote Execution plugin
#
# Parameters:
#
# remote_execution_ssh_keys: public keys to be put in ~/.ssh/authorized_keys
#
# remote_execution_ssh_user: user for which remote_execution_ssh_keys will be
#                            authorized
#
# remote_execution_create_user: create user if it not already existing
#
# remote_execution_effective_user_method: method to switch from ssh user to
#                                         effective user
#
# This template sets up SSH keys in any host so that as long as your public
# SSH key is in remote_execution_ssh_keys, you can SSH into a host. This
# works in combination with Remote Execution plugin by querying smart proxies
# to build an array.
#
# To use this snippet without the plugin provide the SSH keys as host parameter
# remote_execution_ssh_keys. It expects the same format like the authorized_keys
# file.





user_exists=false
getent passwd root >/dev/null 2>&1 && user_exists=true


if $user_exists; then


  mkdir -p ~root/.ssh

  cat << EOF >> ~root/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK4ViMtAG+UOKBpFWSFnARRfydVMze0H3qP/CyOI60hE beatmue
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJeolp0Z8wPeb6vuCaClVPrIBGm44cX3sIqBjnjHPclH mkern
ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAYEAhsrIP7evOAd+l7uzclDRfHQ7SMkyPzAGKx9W0KIu7v9pmvGMiuPLOYIgYm9aTDs6ArsbuT+rCZpnwsAbwWH2jPaZXkgiwkHHRaiJ6HX6tpChfLwAi0zGiH+Ut2BWZb/Jh8A21mW+B3KPfzNBIAQAZgUp3HURmtBx4pIZkE5+YDYLDPfB1jJ0ZTlnSijqwDm9N8aYKmQ567lnAVMYH0Xh+7cPYtNamJbfVyArocBo0r26nNx9cojZXmWKnCs8Yp0/wxYCqVmLjWQYZfFcpNisEHT4lPQevaLZP+F4onOBGuPEVOptxiRQ/DGNRTv2gGXHeuKIWznc6HDLZU2DOWVpmtsdMBsfP8UssasABxKc3kDiGI9kzgmj9gB4+GqoWobDF61MT2gOwXWRQXi5P5beiLk1lu/f4qaKQEu3088f2oaC8QyKAQwfrS1mresgTDQNpa9tK9VY6DeKQpc366cVSyGZdcswpsNgXa1pfMt3ba/GkM86djMAg5r9sszEUqdz bgiger@ethz.ch
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDrU3bfGL6XR30bV4ZFknQrxbLeqN/5teKQCcwHoHD2x miodermatt
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINjmO0RZBBJZj3I1srIgg6jJyGTJoXBBNzePgiv1ftaY thluethi
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKVmLPNJHqyeDsMR9uoq+2Z8mJU4w9dghJhjUP1u6buK fsteger
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDN1MkSKknFX0g09GF5T0QisGUgrsIg2tUxKF27HXiePNmbxsQXf20fBuPqvwAaGNaG/JyBtV4Pn6sgJvAi/Tkk7vu2FQCh/CL2fGhjQKD8ap9ieOCpVYIXTUL0d423zL1lIyf/1XUtYXeexoKv/SjTGXtSitntVQ7NO/aE1goeLz9x85K+z3JNwy4iNVlbGSHbBQsDj5ZP5zXPJoJNw0eiLGMCzWfAhs8NUpdP0nPA8hKiBD/cJmNS8Od+CkIfz4A+9pwgl3GR+6pxn8p0U6bUsSwKHWEbMeksbumyFd9EEMs1bq5sNsNdVo+B/G/YZ7oIULln3H5yxgGlTlOdcYQKBRz8Tq4hfN28tyNiR2dblHd1o+U/MMDJajRQXRxUCkhgHAi7IXUw/uFgC5/RVSnQIBF47K/qbLo5SWBbXfpZff8EeRbcRrQOxfmM0N8W2BavxQB5GNftXUVAjqV4empf8BWjIED7gra5D0z62J9s3noBxVKEacexlZ11q688FXvH3qoT+kggHP2ykiFwH/QCDbWeSNttPDx/nP2WlorRrvCyHE4mT5BVDQpTGQiUl14DxeQIygEXRi3hYyaa4J97bdjuXcqck6kPPKaSo8UbsmxLjlzhvNw1yOGcjOvcMM8hf1OJr2nZMhA9yboKPS1VsHbTwgMn3ccrcHtYl+v7eQ== ansible
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDAv6Tbjm4HA5BekhB9kr1T5WRgeq6ioUUb3VlWrTay4Z/ruFJ88yVmHm46/yaTbNqMtJaQUkyER9kMmOlGHZhkNjAqNswS3H8ltYcv84vccc/d89DGMoj5MLbGoiS1bEPicoM41Hk4p4oa662aNQ4HRlUBHzlLcnTvo1YRO5YjKu+PUukxmc8zWYco9nvg0giA5Z0AUwVDZpZb5yVokcC8p+BhVRspyx/lLBzZXgif933bmh3MIsk88PGF4Z3LVfefcZvm1TaYzC/syhMILVxcjNjGu0g8hTuMvCi/nrMcfSy9z9Ri56Qu/RExCfyuPLGPj1dpzECSaNlICeyOoflN foreman-proxy@id-sat-prd.ethz.ch
EOF

  chmod 0700 ~root/.ssh
  chmod 0600 ~root/.ssh/authorized_keys
  chown -R root: ~root/.ssh

  # Restore SELinux context with restorecon, if it's available:
  command -v restorecon && restorecon -RvF ~root/.ssh || true

else
  echo 'The remote_execution_ssh_user does not exist and remote_execution_create_user is not set to true.  remote_execution_ssh_keys snippet will not install keys'
fi















%end

%post --erroronfail

sync
%end